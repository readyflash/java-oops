class Whatsappv2 extends Whatsappv1{
    String  status;
    Whatsappv2(long cno,String name)
    {
         super(cno,name);
    }
    Whatsappv2(long cno,String name,String status)
    {
      super(cno,name);
      this.status=status;
    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status){
        this.status=status;
    }
}